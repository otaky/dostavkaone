var pagesUrl = require("../../module/pagesUrl");

Feature('ссылки в подвале страницы');

Before((I) => { // or Background
    I.amOnPage('/');

});

let f = "#footer";

Scenario(' политика конфеденциальности ', (I) => {
    I.checkLinkOpensInNewTab(f, "Политика конфиденциальности", pagesUrl.config.footer.PrivacyPolicy);
});

Scenario(' Пользовательское соглашение ', (I) => {
    I.checkLinkOpensInNewTab(f, "Пользовательское соглашение", pagesUrl.config.footer.LicenseAgreement);
});

Scenario('вконтакте ', (I) => {
    I.checkLinkOpensInNewTab(f, "vk", pagesUrl.config.footer.Vk);
});

Scenario('instagram ', (I) => {
    I.checkLinkOpensInNewTab(f, "#footer > div > div > div.col-xs-6 > ul > li:nth-child(2) > a", pagesUrl.config.footer.Instagram);
});

Scenario('viber ', (I) => {
    I.checkLinkOpensInNewTab(f, "#footer li:nth-child(1) > a", pagesUrl.config.footer.Viber);
});

Scenario('wats up ', (I) => {
    I.checkLinkOpensInNewTab(f, "#footer > div > div > div:nth-child(1) > ul > li:nth-child(2) > a", pagesUrl.config.footer.WutsUp);//все :3
});