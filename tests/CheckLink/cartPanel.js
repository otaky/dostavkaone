var pagesUrl = require("../../module/pagesUrl");//относительный путь, поднялись выше на 2 директории

Feature('навигация панель ресторанов переход на страницу');


Before((I) => { // or Background
    I.amOnPage('/');
});

let rm = "#cart-panel";

Scenario('Big Yorker, полное меню ', (I) => {

    I.checkLinkInOpensInCurrentTab(rm, "Big Yorker", pagesUrl.config.urls.BigYorker );
    I.goToFullMenuInSidebar();
});

Scenario('Boho , полное меню', (I) => {

    I.checkLinkInOpensInCurrentTab(rm, "Boho", pagesUrl.config.urls.Boho);
    I.goToFullMenuInSidebar();
});

Scenario( "Bangkok , полное меню" , (I) => {

    I.checkLinkInOpensInCurrentTab(rm, "Bangkok", pagesUrl.config.urls.Bangkok);
    I.goToFullMenuInSidebar();
});

Scenario( "dostavkaone , полное меню" , (I) => {

    I.checkLinkInOpensInCurrentTab(rm, "Boho", pagesUrl.config.urls.Boho);
    I.checkLinkInOpensInCurrentTab(rm, "Доставка №1", pagesUrl.config.urls.Dostavka);
    I.goToFullMenuInSidebar();
});