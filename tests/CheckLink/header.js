var pagesUrl = require("../../module/pagesUrl");

Feature('шапка сайта, переход на страницу');

Before((I) => {
    I.amOnPage('/');
});

let h = "#header";

Scenario("контакты", (I) => {
    I.checkLinkInOpensInCurrentTab(h, "Контакты", pagesUrl.config.pageNames.Contacts);
});

Scenario('доставка и оплата ', (I) => {
    I.checkLinkInOpensInCurrentTab(h, "Доставка и оплата", pagesUrl.config.pageNames.Delivery);
});

Scenario('акции  ', (I) => {
    I.checkLinkInOpensInCurrentTab(h, "Акции", pagesUrl.config.pageNames.Actions);
});

