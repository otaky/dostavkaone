var buy = require("../../module/buy");
var pagesUrl = require("../../module/pagesUrl");
Feature('Если без покупки зайти на страницу');

Before((I) => {
    I.amOnPage("/");
});

Scenario('Корзина , видим сообщение о пустой корзине ', (I) => {
    I.amOnPage(pagesUrl.config.pageNames.cart);
    I.waitForText("Ваша корзина пуста!", 5 , "#cart");
});

Scenario("оформить заказ , видим сообщение о пустой корзине" , (I) =>{
    I.amOnPage(pagesUrl.config.pageNames.checkout);
    I.waitForText("Ваша корзина пуста!", 5 , "#cart");
});

Scenario('тест должен провалиться , нельзя оформить пустую корзину!', async (I) => {

    await I.amOnPage(pagesUrl.config.pageNames.cart);
    await I.click("Оформить заказ");

});

Scenario('а с покупкой корзину оформить можно!', async (I) => {

    await buy.buyCake970(I);
    await I.click("#tempId");
});
