var database = require("../../module/database");

Feature ("test search");

Data(database.for.searchRequest).Scenario('from page', async ( I, current) => {

    I.amOnPage(current.onPage);

    within("#stickySidebar" , async () =>{
        await I.fillField("#searchQuery" , current.lookingFor);
        await I.pressKey("Enter");
        await I.waitInUrl('/search', 2);
    });

    await I.waitForText(current.expectedResult, 5, '.section-header');
});

