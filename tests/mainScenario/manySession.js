var pages = require("../../module/pagesUrl.js");

Scenario( '3 user !', (I) => {

    session('vasya');
    session('petya');
    session('masha');

    session('vasya', ()=> {
        I.amOnPage(pages.config.pageNames.Delivery);
    });

    session('petya', () => {
        I.amOnPage(pages.config.urls.BigYorker);
    });

    session('masha', () => {
        I.amOnPage(pages.config.pageNames.cart);
    });

});