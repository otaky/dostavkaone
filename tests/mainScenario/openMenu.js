var pagesUrl = require("/home/otaky/snap/webstorm/dostavkaone/module/pagesUrl.js");
var database = require("/home/otaky/snap/webstorm/dostavkaone/module/database.js");

Feature ("user open menu");

Before ((I)=>{
    I.amOnPage(pagesUrl.config.pageNames.fullmenu)
});

Data(database.for.fullmenuPages).Scenario('from page fullmenu', ( I, current) => {
    I.checkLinkOpensInNewTab(".section-container", current.links , current.urls);
});

