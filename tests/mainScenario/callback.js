var database= require('../../module/database.js');

Feature (" test callback vidget ");

Data(database.for.pages).Scenario('on all pages', async ( I, current) => {

    await I.amOnPage(current.pages);

    await I.click("#header > div > div.header-contacts > a");//заказать звонок
    await I.waitForVisible(".modal-body" , 3);//ждём модалку
    await I.click("#callback-form > div.buttons > input");//отправить пустую форму!
    await I.dontSeeElement("#callback-success", 3);

    within("#callback-form" , async  () =>{
        await I.waitForText("Необходимо заполнить «Имя».", 3,       "#callback-form > div");
        await I.waitForText("Необходимо заполнить «Телефон»." , 3 , "#callback-form > div");
        await I.fillField("#callback-name" , "Vasya");
        await I.waitForText("", 3,       "#callback-form > div");
        await I.fillField("#callback-phone" , "333 333-33-33");
        await I.waitForText("", 3,       "#callback-form > div");
        await I.click('input[type=submit]');
    });

    await I.waitForText("Оператор вам перезвонит", 3, ".message");
});

