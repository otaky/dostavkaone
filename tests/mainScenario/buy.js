var goToPage = require("../../module/goToPage");
var buy = require("../../module/buy");

Feature('основной сценарий ');

Before((I) => {
    I.amOnPage('/');
});

Scenario(" оплата картой " , async  (I) => {

    await buy.buyCake970(I);
    await goToPage.cart(I);
    await goToPage.checkout(I);
    I.inputDeliveryData();//вызов с параментрами по умолчанию
    await goToPage.sberbank(I);
    I.inputPaymentRequisites();
    await I.waitForText('Ваш заказ подтвержден!', 30, '.section-title');//изменено с ваш заказ оплачен!

});

