var pagesUrl = require("./pagesUrl.js");

let pages = new DataTable(["pages"]);

pages.add([pagesUrl.config.urls.Dostavka]);
pages.add([pagesUrl.config.urls.BigYorker]);
pages.add([pagesUrl.config.urls.NewYork]);
pages.add([pagesUrl.config.urls.Bangkok]);
pages.add([pagesUrl.config.urls.Boho]);

pages.add([pagesUrl.config.pageNames.About]);
pages.add([pagesUrl.config.pageNames.Contacts]);
pages.add([pagesUrl.config.pageNames.Actions]);
pages.add([pagesUrl.config.pageNames.fullmenu]);
pages.add([pagesUrl.config.pageNames.Delivery]);

let fullmenuPages = new DataTable(["links", "urls"]);

fullmenuPages.add(['Основное меню кафе-бара "New York"', pagesUrl.config.fullmeny.NewYourkMainMenu]);
fullmenuPages.add(['Детское меню кафе-бара "New York"',  pagesUrl.config.fullmeny.NewYourkChildrenMenu]);
fullmenuPages.add(['Меню завтраков кафе-бара "New York"',pagesUrl.config.fullmeny.NewYorkBreakfast]);
fullmenuPages.add(['Основное меню ресторана "Bangkok"',  pagesUrl.config.fullmeny.BangkokMainMenu]);
fullmenuPages.add(['Детское меню ресторана "Bangkok"',   pagesUrl.config.fullmeny.BangkokChildrenMenu]);
fullmenuPages.add(['Основное меню ресторана "Boho"',     pagesUrl.config.fullmeny.BohoMainMenu]);

let searchRequest = new DataTable(['onPage', 'lookingFor', 'expectedResult']);

searchRequest.add([ pagesUrl.config.urls.Dostavka, 'Аляска', 'Аляска']);
searchRequest.add([ pagesUrl.config.urls.BigYorker, 'Аляска', 'Аляска']);
searchRequest.add([ pagesUrl.config.urls.Bangkok, 'Аляска', 'Аляска']);
searchRequest.add([ pagesUrl.config.urls.Boho, 'Аляска', 'Аляска']);
searchRequest.add([ pagesUrl.config.urls.SingleProductPage, 'Аляска', 'Аляска']);

module.exports = {
    for: {
        pages , fullmenuPages , searchRequest
    }
};
