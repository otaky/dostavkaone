//переходы по страницам

// в корзину, ждём изменение ulr
let cart = async (I)  => {

    await I.click("#tempId");
    await I.click("Оформить заказ");
    await I.waitInUrl("store/cart", 30);
};

// оформление заказа, ждём изменение ulr
let checkout = async (I) => {

    await I.waitForElement(".controls", 5);
    await I.click("Оформить заказ");
};

// сбербанк, ждем старницу, видим что отображается в title
let sberbank = async (I) => {

    await I.waitForVisible("#loading-success" , 30);// переход к сберу
    await I.click("#loading-success > a");
    await I.seeInTitle("Платежная страница Сбербанка");
};


module.exports = {
    checkout, cart, sberbank
};
