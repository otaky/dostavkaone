
const getAbsoluteUrl = (uri) => `${baseUrl}${uri}`;
const  baseUrl = 'http://www.dostavkaone.ru';//process.env.baseUrl

const config = {

    urls: {
        BigYorker:         getAbsoluteUrl('/catalog/big-yorker'),
        Boho:              getAbsoluteUrl('/catalog/restoran-domashney-kuhni-boho'),
        Bangkok:           getAbsoluteUrl("/catalog/bangkok"),
        NewYork:           getAbsoluteUrl("/catalog/nyu-york"),
        Dostavka:          getAbsoluteUrl("/catalog/dostavka-no1"),
        SingleProductPage: getAbsoluteUrl(("/catalog/product/picca-karbonara"))
    },

    pageNames: {
        Contacts: getAbsoluteUrl("/home/contacts"),
        Delivery: getAbsoluteUrl("/home/delivery"),
        Actions:  getAbsoluteUrl("/action"),
        About:    getAbsoluteUrl("/home/about"),
        fullmenu: getAbsoluteUrl("/home/fullmenu"),
        cart:     getAbsoluteUrl("/store/cart"),
        checkout: getAbsoluteUrl("/store/checkout")
    },

    footer: {
        PrivacyPolicy:    ("http://www.krasrest.ru/wp-content/uploads/2018/03/Политика-конфиденциальности.pdf"),
        LicenseAgreement: ("http://www.krasrest.ru/wp-content/uploads/2018/03/Пользовательское-соглашение.pdf"),
        Vk:               ("https://vk.com/dostavkaone"),
        Instagram:        ("https://www.instagram.com/dostavkaone/"),
        Viber:            ("viber://add?number=73912190101"),
        WutsUp:           ("https://api.whatsapp.com/send?phone=73912190101")

    },

    fullmeny: {
        NewYourkMainMenu:     getAbsoluteUrl("/storage/new-york-menu-2018_219ec85a.pdf"),
        NewYorkBreakfast:     getAbsoluteUrl("/storage/2018_4246f69e.pdf"),
        NewYourkChildrenMenu: getAbsoluteUrl("/storage/detskoe-menupdf_9c03db83.pdf"),
        BohoMainMenu:         getAbsoluteUrl("/storage/2018web_130e30e3.pdf"),
        BangkokMainMenu:      getAbsoluteUrl("/storage/2018_efc5f262.pdf"),
        BangkokSibireanMenu:  getAbsoluteUrl("/storage/menu-sibir_5e9de8db.pdf"),
        BangkokChildrenMenu:  getAbsoluteUrl("/storage/menu-kids-bangkok_dc42eb3e.pdf")
    },
    callback: {
        callback: ("#header a.callback")
    },
    instagram: {
        dostavkaone: ("https://www.instagram.com/dostavkaone/"),
        bigYorker:   ("https://www.instagram.com/bigyorker/"),
        boho:        ("https://www.instagram.com/bohorestaurant/"),
        newyork:     ("https://www.instagram.com/bar_newyork/"),
        bangkok:     ("https://www.instagram.com/restaurantbangkok/")
    }
};

module.exports = {
    getAbsoluteUrl, config , baseUrl
};
