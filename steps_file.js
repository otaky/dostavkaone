'use strict';

// расширяем методы для абстракции 'I'
var pagesUrl = require("./module/pagesUrl.js");// . -текущий каталог

module.exports = function() {
return actor({

    // Define custom steps here, use 'this' to access default methods of I.
    // It is recommended to place a general 'login' function here.

    //вводим данные для доставки, где valueOption - варианты доставки (1 наличные, 2 картой курьеру, 3 онлайн)
    //указаны параметры по-умолчанию
    inputDeliveryData: function (name = "Vasya", lastname = "Pypkin", street = "Авиаторов", house = "55", phone = "3333333333", valueOption = "3") {
        this.fillField('#orderform-firstname', name);
        this.fillField('#orderform-lastname', lastname);
        this.fillField('#orderform-street', street);
        this.fillField('#orderform-house', house);
        this.fillField('#orderform-phone', phone);
        // this.selectOption('#pt3', valueOption);//ошибка
        if ( valueOption === 1 ) {
          this.click("#order-stage-2 > div > div > div.ordering-wrapper > div.ordering-details > ul.payment-options-list > li:nth-child(1) > div > label");//Наличными курьеру
        }
        if ( valueOption === 2 ) {
          this.click("#order-stage-2 > div > div > div.ordering-wrapper > div.ordering-details > ul.payment-options-list > li:nth-child(2) > div > label");//картой курьеру
        }
        if ( valueOption === 3 ) {
          this.click("#order-stage-2 > div > div > div.ordering-wrapper > div.ordering-details > ul.payment-options-list > li:nth-child(3) > div > label");//онлайн
        }

        this.click('Оформить');
        },

        purchaseItem: function() { // задача вернуть функцию столкьо раз сколько указано в аргументе

        this.click("#wok > ul > li:nth-child(1) > div > div.content > div.details > div.buttons > a");

        },

    //вводим данные карты, сделаем вызов с параментрами по умолчанию
    inputPaymentRequisites: function (email = "vasyaPupkin@mail.ru", cardNumber = "4111 1111 1111 1111", DdMm = "1219", civ2 = "123", verificationcode = "12345678") {
        this.fillField("#email",email );
        this.fillField("#cardnumber", cardNumber );
        this.fillField("#expiry", DdMm );
        this.fillField("#cvc", civ2);
        this.wait(2);
        this.waitForElement(".sbersafe-pay-button", 30);
        this.click("#wrapper > main > section.sb_sbersafe-pay.clearfix > div.col-sm-6 > button");
        this.waitForElement(".value" , 30);
        this.fillField(".value > input" , verificationcode);
        this.click("Submit" );

    },

    //ввода данных для обратного звонка, с параметрами по умолчанию
    inputCallbackForm: function (name = "Вася", phone = "3333333333") {
        this.fillField("#callback-name", name);
        this.fillField("#callback-phone", phone);
        this.pressKey("Enter");

    },

    // для проверки ссылок которые должны открываться в новом окне
    // нужно вветси месо по id или class текст ссылки и что ожидаем в новом окне
    checkLinkOpensInNewTab: function (where, thatClick, url){
        within( where, async () => {
            this.click(thatClick);
            this.switchToNextTab(1 , 5);
            await this.waitInUrl(url , 5);
        });
    },

    // для проверки ссылок которые должны открываться в ТЕКУЩЕМ
    // нужно вветси месо по id или class текст ссылки и что ожидаем в ТЕКУЩЕМ
    checkLinkInOpensInCurrentTab: function (where, thatClick, url) {
        within ( where , () => {
            this.click(thatClick);
            this.waitInUrl(url , 5);
        });
    },

    //переход в полное меню из бокового меню
    goToFullMenuInSidebar: function () {
        within ("#stickySidebar" , () => {
            this.click("Полное меню");
            this.waitInUrl(pagesUrl.config.pageNames.fullmenu, 30);
        });
    },

    //other metods
    checkInsta: function ( clickable_element , expectedUrl){

        this.click(clickable_element);
        this.switchToNextTab(1 , 5);
        this.waitInUrl( expectedUrl , 5);

    },
});
};
